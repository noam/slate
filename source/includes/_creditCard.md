# Credit card integrations

The endpoints below deal with connecting to credit card gateways in order to process a payment.
Usually each organization will use a different gateway but we will try to abstract the methods since most of the workflows are the same.

## Get Session key

> Request example

```json
{
	"organizationId":"a511a378-ce96-4de1-b7b9-c2e9f180cd99"
}
```

> Response example

```json
{
    "statusCode": 0,
    "message": null,
    "status": "SUCCESS",
    "organizationId": "a511a378-ce96-4de1-b7b9-c2e9f180cd99",
    "sessionKey": "D78CC129-916B-41ED-9132-342BB7183E5E"
}
```

This endpoint is used to acquire a session key in order to authenticate the session with the gateway's servers

### HTTP request

`POST https://example.com/transactionNew/getMsk`

### Request parameters

Parameter | Description
--------- | -----------
organizationId | The ID of the organization that is going to process the credit card transaction

### HTTP response

Parameter | Description
--------- | -----------
status | SUCCESS or FAIL
statusCode | 0 of success otherwise a code describing the error
message | null on success, description of error otherwise
organizationId | The ID of the organization that is going to process the credit card transaction
sessionKey | The session key returned from the gateway's server, this key will be used in requests to the gateway later on.
