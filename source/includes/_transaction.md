## Find transaction by deposit code

> Request example

```json
{
  "depositCode":"12345678"
}

```

> Response example

```json
{
    "statusCode": 0,
    "message": null,
    "status": "SUCCESS",
    "senderId": "19c1513d-1d57-4caa-bd8d-ffd3ff70cb90",
    "transactionId": "20e081b0-2202-42c5-8481-0bde969b3bdd",
    "depositCode": "5473282489",
    "amountSent": "700.00",
    "currency": "EUR",
    "amountReceived": "51582",
    "receiveCurrency": "INR",
    "payoutType": "BANK_ACCOUNT",
    "recipientData": {
        "statusCode": 0,
        "message": null,
        "status": "SUCCESS",
        "recipientId": "c76cb4e3-1deb-4871-b37b-d73f0dbd1f88",
        "firstName": "Robute",
        "lastName": "Guliiam",
        "phoneNumber": "+919812349900",
        "country": "IN",
        "dateOfBirth": "1985-07-22",
        "ifscCode": "ICIC000321",
        "accountNumber": "101267",
        "bankName": "ICIC Bank",
        "bankBranchName": "Mumbai branch",
        "bankAddress": null,
        "bankCity": "Mumbai",
        "bankDistrict": null,
        "bankState": null,
        "bankCode": "9100041"
    },
    "senderData": {
        "firstName": "Sebastian",
        "lastName": "Yarik",
        "phone": "+33523111000",
        "country": "FR",
        "dateOfBirth": "1985-09-22",
        "city": "Paris",
        "address": "Rue de Rue",
        "idNumber": "1234567",
        "idType": "PASSPORT"
    }
}
```

This endpoint returns a transaction in PRE_TXN status that has the given deposit code.
Requires user to have role AGENT or ADMIN

### HTTP Request

`POST https://example.com/transactionNew/deposit`

### Request parameters

Parameter | Description
--------- | -----------
depositCode | The deposit code to search by

### HTTP response

Parameter | Description
--------- | -----------
status | SUCCESS or FAIL
statusCode | 0 of success otherwise a code describing the error
message | null on success, description of error otherwise
senderId | The sender ID
transactionId | The transaction ID
depositCode | The transaction deposit code
amountSent | The amount sent in source currency
currency | The source currency
amountReceived | The amount received in destination currency
receiveCurrency | The destination currency
payoutType | The transaction payout type, can be CASH or BANK_ACCOUNT
recipientData | The recipient data (see [Find recipients](#find-recipients) method for recipient object data)
senderData | The sender data (see [Find user](#find-user) method for sender object details)    

## Find transaction by code

> Request example

```json
{
	"queryType":"RESERVATION",
	"queryCode":"0067720038"
}
```

> Response example

```json
{
    "statusCode": 0,
    "message": null,
    "status": "SUCCESS",
    "queryType": "RESERVATION",
    "queryCode": "0067720038",
    "transactionDetails": {
        "transactionStatus": "PROCESSING",
        "depositCode": "4460248007",
        "reservationCode": "0067720038",
        "amount": 700,
        "amountDest": 51567,
        "sourceCurrency": "EUR",
        "destinationCurrency": "INR",
        "payoutType": "CASH",
        "transactionDate": "2017-07-09 15:14",
        "sender": {
            "id": "ad6cb4e3-1deb-4871-b37b-d73f0dbd1f88",
            "firstName": "Sebastian",
            "lastName": "Yarik",
            "dateOfBirth": "1985-09-22",
            "city": "Paris",
            "address": "Rue de Rue",
            "country": "FR",
            "phoneNumber": "+33523111000",
            "idNumber": "1234567"
        },
        "receiver": {
            "id": "c76cb4e3-1deb-4871-b37b-d73f0dbd1f88",
            "firstName": "Robute",
            "lastName": "Guliiam",
            "dateOfBirth": "1985-07-22",
            "city": null,
            "address": null,
            "country": "IN",
            "phoneNumber": "+919812349900",
            "idNumber": null
        },
        "bankName": "ICIC Bank",
        "bankBranch": "Mumbai branch",
        "bankAccountNumber": "101267",
        "ifscCode": "ICIC000321",
        "priceResponse": {
            "statusCode": 0,
            "message": null,
            "status": "SUCCESS",
            "organizationId": null,
            "amountSentSRC": 400,
            "fxCommissionSRCUSDPercent": 0.005,
            "fxCommissionUSDDESTPercent": 0.01,
            "organizationFeePercent": 0,
            "organizationFeeFixed": 7,
            "fxRateSRCUSD": 3.539302,
            "fxRateUSDDEST": 65.418999,
            "fxRateSRCUSDWithCommission": 3.556999,
            "fxRateUSDDESTWithCommission": 64.764809,
            "amountSentInUSD": 113.016634,
            "organizationFeeAmountInUSD": 7,
            "organizationFeeFXSRCInUSD": 0.565083,
            "organizationFeeFXDESTInUSD": 1.130166,
            "reconciliationAmountUSD": 0.009181,
            "amountReceivedUSD": 104.312204,
            "amountReceivedDEST": 6824,
            "organizationRevenueUSD": 8.70443,
            "organizationTotalFeePercent": 0.077019,
            "fsCostFixedUSD": 0,
            "fsCostPercent": 0
        }
    }
}
```

This endpoint is used to query transaction details, this endpoint can be used to query transactions in all statuses.

### HTTP Request

`POST https://example.com/transactionNew/queryByCode`

### Request Parameters

Parameter | Description
--------- | ----------
queryType | The code type to query by, can be `DEPOSIT` or `RESERVATION`
queryCode | The code to query

### HTTP Response

The response returns the transaction details.

Parameter | Description
--------- | ----------
status | SUCCESS or FAIL
statusCode | 0 of success otherwise a code describing the error
message | null on success, description of error otherwise
queryType | The code type to query by, can be `DEPOSIT` or `RESERVATION`
queryCode | The code to query
transactionDetails | an object containing the transaction details (see below)

### Transaction details object

Parameter | Description
--------- | ----------
transactionId | The transaction ID
transactionStatus | The transaction status
depositCode | The transaction deposit code
reservationCode | The transaction reservation code
amount | The transaction amount in source currency
amountDest | The transaction amount in destination currency
sourceCurrency | The source currency
destinationCurrency | The destination currency
payoutType | The transaction payout type, can be `CASH` or `BANK_ACCOUNT`
transactionDate | tHe transaction date, given in `yyyy-MM-dd HH:mm` format
bankName | The bank name (null incase of cash transaction)
bankBranch | The bank branch name (null incase of cash transaction)
bankAccountNumber | The bank account numbet (null incase of cash transaction)
ifscCode | The  IFSC code (null incase of cash transaction or recipient not from india)
sender | the sender details, (see below)
receiver | The recipient details, (see below)
priceResponse | The transaction price Data see [Price request](#price-request) for object details.

### User details object

Parameter | Description
--------- | ----------
id | The user's ID in the system
phoneNumber | The users phone number formatted in E164 format.
firstName | The users first name
lastName | The users last name
country | The users country in 2 letter ISO code, e.g. IN = India, PH = Philippines
dateOfBirth | The users date of birth
address | The users address
city | The users city
idNumber | the users ID number

## Create new transaction

> Request example

```json
{
	"senderId":"19c1513d-1d57-4caa-bd8d-ffd3ff70cb90",
	"recipientId":"c76cb4e3-1deb-4871-b37b-d73f0dbd1f88",
	"amount":"700",
	"sendCurrency":"EUR",
	"receiveCurrency":"INR",
	"fundingSource":"CASH",
	"payoutType":"CASH",
	"pickupCode": "CPMUTFI",
	"organizationId":"9b53803f-1818-466b-b6bd-393fcc33e67a"
}

```

> Response example

```json
{
    "statusCode": 0,
    "message": null,
    "status": "SUCCESS",
    "organizationId": "9b53803f-1818-466b-b6bd-393fcc33e67a",
    "eventId": "6e35997b-e5a3-4d3e-8ea9-90f565c7c540",
    "transactionId": "6e35997b-e5a3-4d3e-8ea9-90f565c7c540",
    "senderId": "19c1513d-1d57-4caa-bd8d-ffd3ff70cb90",
    "amount": "700",
    "sendCurrency": "EUR",
    "receiveCurrency": "INR",
    "fundingSource": "CASH",
    "transactionStatus": "PRE_TXN",
    "priceResponse": {
        "statusCode": 0,
        "message": null,
        "status": "SUCCESS",
        "organizationId": null,
        "amountSentSRC": 700,
        "fxCommissionSRCUSDPercent": 0,
        "fxCommissionUSDDESTPercent": 0,
        "organizationFeePercent": 0,
        "organizationFeeFixed": 0,
        "fxRateSRCUSD": 0.876904,
        "fxRateUSDDEST": 64.599998,
        "fxRateSRCUSDWithCommission": 0.876904,
        "fxRateUSDDESTWithCommission": 64.599998,
        "amountSentInUSD": 798.26298,
        "organizationFeeAmountInUSD": 0,
        "organizationFeeFXSRCInUSD": 0,
        "organizationFeeFXDESTInUSD": 0,
        "reconciliationAmountUSD": 0.012181,
        "amountReceivedUSD": 798.250799,
        "amountReceivedDEST": 51567,
        "organizationRevenueUSD": 0.012181,
        "organizationTotalFeePercent": 0.000015,
        "fsCostFixedUSD": 0,
        "fsCostPercent": 0
    },
    "depositCode": "4460248007",
    "payoutType": "CASH",
    "recipientId": "c76cb4e3-1deb-4871-b37b-d73f0dbd1f88",
    "depositingAgentId": null,
    "depositingUserId": null,
    "pickupCode": "CPMUTFI"
}
```

This endpoint creates a new transaction in the system, the transaction returned is always in PRE_TXN status and is ready for confirmation

### HTTP Request

`POST https://example.com/transactionNew/new`

### Request Parameters

Parameter | Description
--------- | ----------
senderId | The sending user ID
recipientId | The recipient ID
amount | The transaction amount in source currency
sendCurrency | The source currency
receiveCurrency | The destination currency
fundingSource | The transaction funding source, can be `CASH` or `CREDIT_CARD`
payoutType | The transaction payout type, can be `CASH`, `BANK_ACCOUNT`, `E_ZWITCH`, `MOBILE_TOPUP` and `BILL_PAYMENT`
pickupCode (optional) | For `CASH` transactions, this is the pickup location code.
cashPickupName (optional) | For `CASH` transactions, this is the pickup location name
mobileOperatorName (optional) | For mobile topup transactions, this is the name of the mobile operator
organizationId | The organization ID that is sending the transaction

### HTTP Response

The response returns the transaction details including the deposit code.

Parameter | Description
--------- | ----------
status | SUCCESS or FAIL
statusCode | 0 of success otherwise a code describing the error
message | null on success, description of error otherwise
transactionId | The ID of the new transaction
senderId | The sending user ID
recipientId | The recipient ID
amount | The transaction amount in source currency
sendCurrency | The source currency
receiveCurrency | The destination currency
fundingSource | The transaction funding source, can be `CASH` or `CREDIT_CARD`
transactionStatus | The transaction status, will always be `PRE_TXN`
payoutType | The transaction payout type, can be CASH or `BANK_ACCOUNT`
pickupCode (optional) | For cash transactions, this is the pickup location code.
depositCode | The deposit code associated with this transaction
priceResponse | The price object of the transaction see [Price request](#price-request) for object details.

## Confirm transaction

> Request example

```json
{
  "transactionId":"9d2d35c5-15eb-4de2-99b3-44e98a21139d",
  "agentId":"3ac7f3f1-3e5e-48b1-8173-2c6bc62475f2",
  "depositingUserId":"afd5e732-85c8-4cac-b10e-7519e5061700",
  "organizationId":"9b53803f-1818-466b-b6bd-393fcc33e67a"
}
```

> Response example

```json
{
    "statusCode": 0,
    "message": null,
    "status": "SUCCESS",
    "organizationId": null
}
```

This endpoint is to be used by tellers to confirm a transaction, a successful request means that money was sent to the recipient

### HTTP request

`POST https://example.com/transactionNew/confirm`

### Request Parameters

Parameter | Description
--------- | ----------
transactionId | The transaction ID to confirm
agentId | The ID of the agent that accepted the deposit
depositingUserId | The ID of the user (teller) that accepted the deposit
organizationId | The organization ID that accepted the deposit

### HTTP Response

Parameter | Description
--------- | ----------
status | SUCCESS or FAIL
statusCode | 0 of success otherwise a code describing the error
message | null on success, description of error otherwise

## Cancel transaction

> Request example

```json
{
  "transactionId":"9d2d35c5-15eb-4de2-99b3-44e98a21139d",
}
```

> Response example

```json
{
    "statusCode": 0,
    "message": null,
    "status": "SUCCESS",
    "organizationId": null
}
```

This endpoint is used to cancel a transaction, only cash transactions in `FS_CHARGED` or `PROCESSING` status can be canceled.

### HTTP request

`POST https://example.com/transactionNew/cancel`

### Request Parameters

Parameter | Description
--------- | ----------
transactionId | The transaction ID to cancel

### HTTP Response

Parameter | Description
--------- | ----------
status | SUCCESS or FAIL
statusCode | 0 of success otherwise a code describing the error
message | null on success, description of reason transaction cannot be canceled otherwise
