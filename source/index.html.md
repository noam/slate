---
title: Vala API Reference

language_tabs:
  - json

toc_footers:
  - <a href='https://valapay.com'>Valapay</a>

includes:
  - transaction
  - creditCard
  - agent
  - reports
  - errors

search: true
---

# Introduction

Welcome to the Vala Dashboard API, below are all the calls needed to preform all actions in the dashboard.

The server expects requests in JSON format and all responses are in JSON format as well.

All requests to the server except the login request must have an organization ID supplied as part of the message.
When logging in to the dashboard as a teller the organization ID is inferred by the server and is not needed on the response

All responses from the server return a `status` string and a `statusCode` number.
For successful requests the `status` will be `SUCCESS` and the `statusCode` will be `0`, in a failure the `status` will be `FAIL` and the `statusCode` and `message` fields will be populated.

Below are the expected formats for special fields:

Field type | Format
---------- | -------
Date | All dates are expected to be in 'yyyy-MM-dd' format
Phone number | Phone numbers are to be sent in E164 format (example: +919234567890).
Country | Country should be sent in 2 letter ISO code, e.g. IN = India, PH = Philippines


# Authentication

> To authorize, add the token to the Authorization header:

```typescript
newOptions.headers.set('Authorization', 'bearer ' + token);
```

> Make sure to replace `token` with the token returned from the login request.

Vala uses [JWT](https://jwt.io/) for authentication.
When a user logs in the server replies with a token, you must send this token on each request as an `Authorization` header with the string `bearer {token}`


# API Endpoints

## Login

> Example for a request:

```json
{
  "phoneNumber" : "+972547666311",
  "email" : "my@email.com",
  "password" : "81c02d",
  "organizationId":"9b53803f-1818-466b-b6bd-393fcc33e67a"
}
```

> Response example:

```json
{
  "statusCode": 0,
  "message": null,
  "status": "SUCCESS",
  "phoneNumber": "+972547666311",
  "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIrOTcyNTQ3NjA2Mzc5IiwiYWNjb3VudHMiOlt7ImNvbm5lY3RlZEVudGiwiYWNjb3VudFR5cGUiOiJSRU1fU0VOREVSInWRFbn04MzFlLTIzM2EyOTk0ZmZjMCIsImFjY291bnRUeXBlIjoiQUdFTlQifCwiaWF0IjoxNDk2OTMwNTY0LCJyb2xlcyI6IlVTRVIsQUdFTlRfQURNSU4ifQ==.rHqHrmMgiPmUpkeuLtxSdgFTIM68YhIkazcVfRcekBU=",
  "userId": "b2a37753-7b1e-412d-8e5c-acbf072f60dc",
  "userData": {
    "firstName": "Ravid",
    "middleName": "Roland",
    "lastName": "Cohen",
    "phone": "+972547666311",
    "country": "FR",
    "dateOfBirth": "1988-01-01",
    "email": "aaa@bbb.com",
    "zipCode": "12333",
    "customerCardNumber": "1111",
    "accounts": [
      {
        "accountType": "AGENT",
        "connectedEntityId": "8b70bdf3-41bd-49b3-831e-233a2994ffc0"
      }
    ],
    "recipients": [],
    "organizationData": {
            "organizationId": "9b53803f-1818-466b-b6bd-393fcc33e67a",
            "extraData" : "111222",
            "organizationName": "Unity Services Ltd",
            "organizationAddress": "158-160 Balham High Road SW12 9BN, London",
            "legalName": "Legal Link",
            "acceptedCurrencies": [
                "USD",
                "ILS"
            ],
            "acceptedFundingSources": [
               "CASH"
            ],
            "payoutTypes": [
               "CASH",
               "BANK_ACCOUNT"
            ],
            "destinationCountries": [
                "IN",
                "PH"
            ],
            "originCountries": [
                "FR"
            ],
            "countryOperators": {
                "IN": "387d826d-a6ce-4a23-b178-d71729e6387d",
                "PH": "387d826d-a6ce-4a23-b178-d71729e6387d"
            },
            "transactionLimits": [
                {
                    "payoutType": "DEFAULT",
                    "transactionIDLimit": "500.00",
                    "transactionUpperLimit": "10000.00",
                    "limitCurrency": "GBP"
                },
                {
                    "payoutType": "CASH",
                    "transactionIDLimit": "250.00",
                    "transactionUpperLimit": "5000.00",
                    "limitCurrency": "GBP"
                }
            ]
        }
  }
}
```

This endpoint allows a user to login.
Vala uses a phone number as a main identifier, all phone numbers supplied to server must be formatted in the [E164](https://en.wikipedia.org/wiki/E.164) format, for example a formatted phone number will look like this `+972547666999`.
We also allow logging in with email, in this case leave the `phoneNumber` field empty.

### HTTP Request

`POST https://example.com/unAuth/login`

### Query Parameters

Parameter | Description
--------- | -----------
phoneNumber | The user's phone number formatted in E164 format.
email | The user's email
password | The users password.
organizationId | The organization ID the user belongs to

### HTTP response

Parameter | Description
--------- | -----------
status | SUCCESS or FAIL
statusCode | 0 of success otherwise a code describing the error
message | null on success, description of error otherwise
phoneNumber | The users phone number
token | The authorization token to use in further requests
userId | a UUID of the user
userData | an object containing the user data, see below


### User data object Parameters

Parameter | Description
---- | -----
firstName | The users first name
middleName | The user's middle name
lastName | The users last name
phone | The users phone number
country | The users country in 2 letter ISO code, e.g. IN = India, PH = Philippines
city | The user's city
address | Te user's address
zipCode | The user's zip code
email | The user's email
customerCardNumber | The user's customer card  
dateOfBirth | The users date of birth
idNumber | The user's ID number
idExpiryDate | The user's ID expiry date
accounts | An array of the user accounts, see below for details
recipients | An array of all recipients defined by the user, relevant for users with account type REM_SENDER
organizationData | An object containing data relevant to the current organization, see below.

### Account object Parameters

Parameter | Description
---- | -----
accountType | The type of the account, can be REM_SENDER, AGENT, ORGANIZATION, VALA_ADMIN
connectedEntityId | The id of the account entity

### Organization data object Parameters

Parameter | Description
---- | -----
organizationId | The Id of the organization this user is part of.
organizationName | The organization name
organizationAddress | The organization address
legalName | The organization legal name
extraData | Extra data of the organization, usually used in the receipt and in reports
acceptedCurrencies | An array of the currencies accepted for deposit in this organization in 3 letter ISO code. e.g: USD, INR
destinationCountries | An array of the countries available as destinations from this organization, given in 2 letter is code. e.g. IN, PH
originCountries | An array of the countries available as source countries for this organization, given in 2 letter is code. e.g. IN, PH
countryOperators | A dictionary mapping between a destination country and the operator ID used by this organization to send to that country.
acceptedFundingSources | The funding sources this organization accepts
payoutTypes | The payout types this organization supports
transactionLimits | An object specifying the transaction limits for this organization (see below)

### Transaction limits object
Parameter | Description
---- | -----
payoutType | The payout type this limit applies to (value of DEFAULT applies of no match is found)
transactionIDLimit | Transactions with amount over this limit require the sender to have an ID scan in the system
transactionUpperLimit | Transactions above this limit are not allowed
limitCurrency | The currency in which the limits are applied in

### Available funding sources

`CASH`
`CREDIT_CARD`
`BANK_ACCOUNT_ACH`
`BANK_ACCOUNT_WIRE`

### Available payout types

`CASH`
`BANK_ACCOUNT`
`E_ZWITCH`
`MOBILE_TOPUP`
`BILL_PAYMENT`

## Get Organization Data (un auth)

> Request example

```json
{
	"organizationId":"d9f456b6-604f-489c-b311-4ce1b4f5fb3a"
}
```

> Resposne example

```json
{
    "statusCode": 0,
    "message": null,
    "status": "SUCCESS",
    "organizationId": "d9f456b6-604f-489c-b311-4ce1b4f5fb3a",
    "organizationName": "Unity Services",
    "organizationAddress": "158-160 Thurlow road, London",
    "extraData": "1233122",
    "legalName": "fhfh",
    "hasPOA": "false",
    "acceptedCurrencies": [
        "GBP"
    ],
    "acceptedFundingSources": [
        "CREDIT_CARD"
    ],
    "payoutTypes": [
        "E_ZWITCH",
        "CASH",
        "BANK_ACCOUNT"
    ],
    "destinationCountries": [
        "GH"
    ],
    "originCountries": [
        "GB"
    ],
    "countryOperators": {
        "GH": "7508343d-56b0-47c3-8ddb-86a7e89988e3"
    },
    "transactionLimits": [
        {
            "payoutType": "CASH",
            "transactionIDLimit": "250.00",
            "transactionUpperLimit": "5000.00",
            "limitCurrency": "GBP"
        },
        {
            "payoutType": "DEFAULT",
            "transactionIDLimit": "500.00",
            "transactionUpperLimit": "10000.00",
            "limitCurrency": "GBP"
        }
    ]
}
```


This endpoint is used to get data for an organization

### HTTP Request

`POST https://example.com/unAuth/organization/data`

### Query Parameters

Parameter | Description
--------- | -----------
organizationId | The organization ID the get data for

### HTTP response

An organization data object, see [organizationData](#organization-data-object-parameters) for details

## Get account data

> Request example

```json
{
	"userId":"afd5e732-85c8-4cac-b10e-7519e5061700",
	"organizationId":"9b53803f-1818-466b-b6bd-393fcc33e67a"
}
```

> Response example

```json
{
    "statusCode": 0,
    "message": null,
    "status": "SUCCESS",
    "userData": {
        "firstName": "new",
        "lastName": "value",
        "phone": "+972547111333",
        "country": "IL",
        "dateOfBirth": "1988-06-17",
        "city": "Paris",
        "address": "13 Rue de rue",
        "idNumber": "123456",
        "idType": "ID",
        "accounts": [
            {
                "accountType": "REM_SENDER",
                "connectedEntityId": "afd5e732-85c8-4cac-b10e-7519e5061700"
            },
            {
                "accountType": "AGENT",
                "connectedEntityId": "3ac7f3f1-3e5e-48b1-8173-2c6bc62475f2"
            }
        ],
        "recipients": [
            {
                "recipientId": "fdfcd4db-7abe-4f5d-8cc1-af1b86c857a4",
                "senderId": null,
                "firstName": "india",
                "lastName": "recip",
                "phoneNumber": "+919812345678",
                "country": "IN",
                "dateOfBirth": "1985-07-22",
                "ifscCode": "ICIC000321",
                "accountNumber": "101267",
            },    
        ],
        "organizationData": {
            "organizationId": "9b53803f-1818-466b-b6bd-393fcc33e67a",
            "acceptedCurrencies": [
                "USD",
                "ILS"
            ],
            "destinationCountires": [
                "IN",
                "PH"
            ],
            "originCountries": [
                "FR"
            ],
            "countryOperators": {
                "IN": "387d826d-a6ce-4a23-b178-d71729e6387d",
                "PH": "387d826d-a6ce-4a23-b178-d71729e6387d"
            }
        }
    }
}

```

This endpoint is used to get account data for the user id sent to the server

### HTTP Request

`POST https://example.com/user/accountData`

### Query Parameters

Parameter | Description
--------- | -----------
userId | The user ID to get account data for
organizationId | The organization ID the user belongs to

### HTTP response

The response is a user data object, see [Login](#login) for response parameter details.

## Get account data (token only)

This endpoint is used to get account data according to the token sent on the header

### HTTP Request

`GET https://example.com/user/accountData`

### HTTP response

The response is a user data object, see [Login](#login) for response parameter details.

## Password reset

> Request example

```json
{
	"phoneNumber":"+972546222333",
	"organizationId":"9b53803f-1818-466b-b6bd-393fcc33e67a"
}
```
> Response example

```json
{
    "statusCode": 0,
    "message": null,
    "status": "SUCCESS",
    "organizationId": null
}
```

This end point is used to reset a user's password. In case of a teller or organization user, sending the organization ID is optional


### HTTP Request

`POST https://example.com/unAuth/forgotPassword`

### Request Parameters

Parameter | Description
--------- | -----------
phoneNumber | The users phone number formatted in E164 format.
organizationId (optional) | The organization this user belongs to, optional in case of an organization user.

### HTTP Response

HTTP 200 for success.

HTTP 404 for phone number not found in the DB.

## Send verification code

> Request example

```json
{
	"organizationId":"9b53803f-1818-466b-b6bd-393fcc33e67a",
	"phoneNumber":"+972527606377",
	"country":"IL"
}
```

> Response example

```json
{
    "statusCode": 0,
    "message": null,
    "status": "SUCCESS",
    "organizationId": null
}
```

This endpoint is used to generate a OTP for signup verification, on success anSMS with the OTP will be sent to the given phone number

### HTTP Request

`POST https://example.com/unAuth/sendCode`

### Request Parameters

Parameter | Description
--------- | -----------
phoneNumber | The users phone number formatted in E164 format.
organizationId | The organization the user will belong to
country | The user's country in 2 letter ISO format

### HTTP response

Parameter | Description
--------- | -----------
status | SUCCESS or FAIL
statusCode | 0 of success otherwise a code describing the error
message | null on success, description of error otherwise

## Signup (Authorized)

> Request example

```json
{
  "phoneNumber":"+972523111444",
  "firstName":"sandow",
  "lastName":"doodley",
  "dateOfBirth":"1985-09-22",
  "email":"sad@mail.com",
  "country":"IL",
  "accounts":[{"accountType":"REM_SENDER"}],
  "roles":["USER"],
  "organizationId":"9b53803f-1818-466b-b6bd-393fcc33e67a",
  "address":"13 street RD",
	"city":"Luton",
}
```

> Response example

```json
{
    "statusCode": 0,
    "message": null,
    "status": "SUCCESS",
    "organizationId": "9b53803f-1818-466b-b6bd-393fcc33e67a",
    "email": null,
    "firstName": "sandow",
    "lastName": "doodley",
    "phoneNumber": "+972523111444",
    "country": "IL",
    "city": null,
    "address": null,
    "password": "9wWgf8IIdqK6/xECjtWSajlCIpdGtxVzIZ/lwMXeQXdR4pFrt/DeAOG82PzMFWWF",
    "encryptedPassword": "9wWgf8IIdqK6/xECjtWSajlCIpdGtxVzIZ/lwMXeQXdR4pFrt/DeAOG82PzMFWWF",
    "userId": "2db5f0e2-9cd4-4fc3-90a8-56186be276e6",
    "dateOfBirth": "1985-09-22",
    "idNumber": null,
    "idType": null,
    "idExpiryDate": null,
    "roles": [
        "USER"
    ],
    "accounts": [
        {
            "accountType": "REM_SENDER",
            "connectedEntityId": null
        }
    ],
}
```

This end point allows you to signup a user that is not you. This is to be used in flows that require adding a new user like the remittance flow from the dashboard or the agents management in the dashboard.

### HTTP Request

`POST https://example.com/auth/signupAuth`

### Request Parameters

Parameter | Description
--------- | -----------
phoneNumber | The users phone number formatted in E164 format.
firstName | The users first name
lastName | The users last name
country | The users country in 2 letter ISO code, e.g. IN = India, PH = Philippines
dateOfBirth | The users date of birth
accounts | An array of user accounts, see login endpoint for details.
roles | An array of roles that the user can have. possible values are: ADMIN, USER, AGENT, AGENT_ADMIN, ORG_ADMIN, ORG_USER
organizationId | The Id of the organization this user is part of.
address (optional) | The user's address
city (optional) | The user's city
email (optional) | The users email
idNumber (optional) | The user's identification document number
idType (optional) | The user's identification document type, can be ID, PASSPORT, WORK_PERMIT

### HTTP response

The response returns the user details in the same way as the request, in addition the response contains the new user ID.

Parameter | Description
--------- | -----------
status | SUCCESS or FAIL
statusCode | 0 of success otherwise a code describing the error
message | null on success, description of error otherwise
userId | The new ID for the user
phoneNumber | The users phone number formatted in E164 format.
firstName | The users first name
lastName | The users last name
country | The users country in 2 letter ISO code, e.g. IN = India, PH = Philippines
dateOfBirth | The users date of birth
accounts | An array of user accounts, see login endpoint for details.
roles | An array of roles that the user can have. possible values are: ADMIN, USER, AGENT, AGENT_ADMIN, ORG_ADMIN, ORG_USER
organizationId | The Id of the organization this user is part of.
address (optional) | The user's address
city (optional) | The user's city
email (optional) | The users email,
idNumber (optional) | The user's identification document number
idType (optional) | The user's identification document type, can be ID, PASSPORT, WORK_PERMIT

## Signup (unAuth)

This end point is used to signup an end user to the system, used in the mobile app.
This method sends the same request as [Signup Auth](#signup-authorized) but adds a verification code to the request.
The response is the same

> Request example

```json
{
  "phoneNumber":"+4407123456789",
  "firstName":"sandow",
  "middleName": "archibald",
  "lastName":"doodley",
  "dateOfBirth":"1985-09-22",
  "email":"sad@mail.com",
  "country":"GB",
  "accounts":[{"accountType":"REM_SENDER"}],
  "roles":["USER"],
  "organizationId":"9b53803f-1818-466b-b6bd-393fcc33e67a",
  "address":"13 street RD",
  "city":"Luton",
  "zipCode":"123 EDX",
  "idNumber": "123456",
  "idType": "PASSPORT",
  "idExpiryDate": "2020-07-22",
  "verificationCode":"1111",
  "password":"222222"
}
```

> Response example

```json
{
    "statusCode": 0,
    "message": null,
    "status": "SUCCESS",
    "organizationId": "9b53803f-1818-466b-b6bd-393fcc33e67a",
    "email": null,
    "firstName": "sandow",
    "middleName": "archibald",
    "lastName": "doodley",
    "phoneNumber": "+972523111444",
    "country": "IL",
    "city": null,
    "address": null,
    "password": "9wWgf8IIdqK6/xECjtWSajlCIpdGtxVzIZ/lwMXeQXdR4pFrt/DeAOG82PzMFWWF",
    "encryptedPassword": "9wWgf8IIdqK6/xECjtWSajlCIpdGtxVzIZ/lwMXeQXdR4pFrt/DeAOG82PzMFWWF",
    "userId": "2db5f0e2-9cd4-4fc3-90a8-56186be276e6",
    "dateOfBirth": "1985-09-22",
    "idNumber": "123456",
    "idType": "PASSPORT",
    "idExpiryDate": "22-07-2020",
    "roles": [
        "USER"
    ],
    "accounts": [
        {
            "accountType": "REM_SENDER",
            "connectedEntityId": null
        }
    ],
}
```

### HTTP Request

`POST https://example.com/unAuth/signup`

### Request Parameters

Parameter | Description
--------- | -----------
phoneNumber | The users phone number formatted in E164 format.
firstName | The users first name
middleName (optional) | The user's middle name
lastName | The users last name
country | The users country in 2 letter ISO code, e.g. IN = India, PH = Philippines
dateOfBirth | The users date of birth
accounts | An array of user accounts, see login endpoint for details.
roles | An array of roles that the user can have. possible values are: ADMIN, USER, AGENT, AGENT_ADMIN, ORG_ADMIN, ORG_USER
organizationId | The Id of the organization this user is part of.
address | The user's address
city | The user's city
email (optional) | The users email
idNumber | The user's identification document number
idType | The user's identification document type, can be ID, PASSPORT, WORK_PERMIT
idExpiryDate | The expiry date of the supplied id
verificationCode | a 4 digit code used to verify the user's phone
zipCode | The zip code of the given address

### HTTP response

The response returns the user details in the same way as the request, in addition the response contains the new user ID.

Parameter | Description
--------- | -----------
status | SUCCESS or FAIL
statusCode | 0 of success otherwise a code describing the error
message | null on success, description of error otherwise
userId | The new ID for the user
phoneNumber | The users phone number formatted in E164 format.
firstName | The users first name
lastName | The users last name
country | The users country in 2 letter ISO code, e.g. IN = India, PH = Philippines
dateOfBirth | The users date of birth
accounts | An array of user accounts, see login endpoint for details.
roles | An array of roles that the user can have. possible values are: ADMIN, USER, AGENT, AGENT_ADMIN, ORG_ADMIN, ORG_USER
organizationId | The Id of the organization this user is part of.
address (optional) | The user's address
city | The user's city
email (optional) | The users email,
idNumber | The user's identification document number
idType | The user's identification document type, can be ID, PASSPORT, WORK_PERMIT
idExpiryDate | The expiry date of the supplied id
zipCode | The zip code of the given address


## Get Fees for organization

> Request Example

```json
{
	"organizationId":"a511a378-ce96-4de1-b7b9-c2e9f180cd99"
}
```

> Response example

```json
{
    "statusCode": 0,
    "message": null,
    "status": "SUCCESS",
    "organizationId": "a511a378-ce96-4de1-b7b9-c2e9f180cd99",
    "feeData": [
        {
            "sourceCountry": "IL",
            "destCountry": "IN",
            "depositCurrency": "ILS",
            "minAmount": 1501,
            "maxAmount": 3000,
            "fxCommissionDESTPercent": 0.01,
            "fxCommissionSRCPercent": 0.005,
            "txnFeeFixed": 10,
            "txnFeePercent": 0
        },
        {
            "sourceCountry": "IL",
            "destCountry": "IN",
            "depositCurrency": "ILS",
            "minAmount": 0,
            "maxAmount": 1500,
            "fxCommissionDESTPercent": 0.01,
            "fxCommissionSRCPercent": 0.005,
            "txnFeeFixed": 7,
            "txnFeePercent": 0
        }
    ]
}
```

This endpoint returns the fees set for a specific organization.
A null value in the `sourceCountry`, `destCountry`, `depositCurrency`, `minAmount` or `maxAmount` fields means wildcard (i.e. fits all values)


### HTTP Request

`POST https://example.com/price/getFees`

### Request Parameters

Parameter | Description
--------- | -----------
organizationId | The organization Id we want to query the fees for

### HTTP Response

The response will contain a list of `FeeData` for the given organization

Parameter | Description
--------- | -----------
status | SUCCESS or FAIL
statusCode | 0 of success otherwise a code describing the error
message | null on success, description of error otherwise
organizationId | The organization Id we want to query the fess for
feeData | An array containing the fee data for the given organization (see below)

### Fee Data object

Parameter | Description
--------- | -----------
sourceCountry | The origination country of the corridor
destCountry | The destination country of the corridor
depositCurrency | The deposit currency for this fee
minAmount | The min part of the interval (inclusive) of the deposit amount that this fee applies to in source currency
maxAmount | The max part of the interval (inclusive) of the deposit amount that this fee applies to in source currency
fxCommissionSRCPercent | The FX commission in percent taken on the source currency to USD conversion (if depositCurrency is USD this will be 0)
fxCommissionDESTPercent | The FX commission in percent taken on the USD to destination currency conversion
txnFeeFixed | The fixed transaction fee amount in USD
txnFeePercent | The transaction fee in percent

## Save fees for organization

> Request example

```json
{
	"organizationId":"9b53803f-1818-466b-b6bd-393fcc33e67a",
	"sourceCountry":"IL",
	"destinationCountry":"IN",
	"feeData": [
		{
			"depositCurrency":"ILS",
			"minAmount":"0",
			"maxAmount":"1000",
			"fxCommissionSRCPercent":"0.005",
			"fxCommissionDESTPercent":"0.01",
			"txnFeeFixed":"0",
			"txnFeePercent":"0.01"
		},
		{
			"depositCurrency":"ILS",
			"minAmount":"1001",
			"maxAmount":"10000",
			"fxCommissionSRCPercent":"0.01",
			"fxCommissionDESTPercent":"0.005",
			"txnFeeFixed":"0",
			"txnFeePercent":"0.005"
		},
		{
			"depositCurrency":"USD",
			"minAmount":"0",
			"maxAmount":"400",
			"fxCommissionSRCPercent":"0.005",
			"fxCommissionDESTPercent":"0.01",
			"txnFeeFixed":"7",
			"txnFeePercent":"0"
		},
		{
			"depositCurrency":"USD",
			"minAmount":"401",
			"maxAmount":"10000",
			"fxCommissionSRCPercent":"0.01",
			"fxCommissionDESTPercent":"0.005",
			"txnFeeFixed":"10",
			"txnFeePercent":"0"
		}
	]
}
```

> Response example

```json
{
    "statusCode": 0,
    "message": null,
    "status": "SUCCESS",
    "organizationId": "9b53803f-1818-466b-b6bd-393fcc33e67a",
    "sourceCountry": "IL",
    "destinationCountry": "IN",
    "feeData": [
        {
            "sourceCountry": "IL",
            "destCountry": "PH",
            "depositCurrency": "ILS",
            "minAmount": 0,
            "maxAmount": 1000,
            "fxCommissionDESTPercent": 0.01,
            "fxCommissionSRCPercent": 0.005,
            "txnFeeFixed": 0,
            "txnFeePercent": 0.01
        },
        {
            "sourceCountry": "IL",
            "destCountry": "PH",
            "depositCurrency": "ILS",
            "minAmount": 1001,
            "maxAmount": 10000,
            "fxCommissionDESTPercent": 0.005,
            "fxCommissionSRCPercent": 0.01,
            "txnFeeFixed": 0,
            "txnFeePercent": 0.005
        },
        {
            "sourceCountry": "IL",
            "destCountry": "IN",
            "depositCurrency": "ILS",
            "minAmount": 0,
            "maxAmount": 1000,
            "fxCommissionDESTPercent": 0.01,
            "fxCommissionSRCPercent": 0.005,
            "txnFeeFixed": 0,
            "txnFeePercent": 0.01
        },
        {
            "sourceCountry": "IL",
            "destCountry": "IN",
            "depositCurrency": "ILS",
            "minAmount": 1001,
            "maxAmount": 10000,
            "fxCommissionDESTPercent": 0.005,
            "fxCommissionSRCPercent": 0.01,
            "txnFeeFixed": 0,
            "txnFeePercent": 0.005
        },
        {
            "sourceCountry": "IL",
            "destCountry": "IN",
            "depositCurrency": "USD",
            "minAmount": 0,
            "maxAmount": 400,
            "fxCommissionDESTPercent": 0.01,
            "fxCommissionSRCPercent": 0.005,
            "txnFeeFixed": 7,
            "txnFeePercent": 0
        },
        {
            "sourceCountry": "IL",
            "destCountry": "IN",
            "depositCurrency": "USD",
            "minAmount": 401,
            "maxAmount": 10000,
            "fxCommissionDESTPercent": 0.005,
            "fxCommissionSRCPercent": 0.01,
            "txnFeeFixed": 10,
            "txnFeePercent": 0
        }
    ]
}
```

This endpoint is used to change the fees for an organization, once this method is called the old fees of the given source and destination countries are deleted and replaced with the fees given in the request.
This method requires that a minimum range with `minAmount` = 0 will be sent for each deposit currency.

### HTTP request

`POST https://example.com/price/saveFees`

### Request Parameters

Parameter | Description
--------- | -----------
organizationId | The organization Id we want to save the fees for
sourceCountry | The source country these fees apply to
destinationCountry | The destination country these fees apply to
feeData | An array of Fee Data objects containing the data of the fees, see [Fee Data Object](#fee-data-object) for details

### HTTP Response

Parameter | Description
--------- | -----------
status | SUCCESS or FAIL
statusCode | 0 of success otherwise a code describing the error
message | null on success, description of error otherwise
organizationId | The organization Id we want to save the fees for
sourceCountry | The source country these fees apply to
destinationCountry | The destination country these fees apply to
feeData | An array of Fee Data objects containing the data of the fees, see [Fee Data Object](#fee-data-object) for details

## Get FX rates

> Response example

```json
{
    "USDAED": 3.672598,
    "USDAFN": 68.620003,
    "USDALL": 112.970001,
    "USDAMD": 478.01001,
    "USDANG": 1.769816,
    "USDAOA": 165.093994,
    "USDARS": 17.048998,
    "USDAUD": 1.272902,
}
```

This endpoint returns the FX rates, the rates are updated once every hour.

### HTTP Request

`GET https://example.com/price/getRates`

### HTTP Response

A JSON object with all the rates in USDXXX format, i.e. the rate from USD to the destination currency

## Set rate for organization

> Request example

```json
{
	"organizationId":"9b53803f-1818-466b-b6bd-393fcc33e67a",
	"sourceCurrency":"EUR",
	"destinationCurrency":"GHS",
	"rate":"66.274"
}
```

> Response example

```json
{
    "statusCode": 0,
    "message": null,
    "status": "SUCCESS",
    "organizationId": "9b53803f-1818-466b-b6bd-393fcc33e67a",
    "sourceCurrency": "EUR",
    "destinationCurrency": "GHS",
    "rate": "66.274"
}
```

This endpoint is used by an organization to set a specific FX rate for a currency exchange

### HTTP Request

`POST https://example.com/price/saveOrganizationRate`

### Request Parameters

Parameter | Description
--------- | -----------
organizationId | The organization Id we want to save the rate for
sourceCurrency | The source currency this rate applies to
destinationCurrency | The destination currency this rate applies to
rate | The FX rate to apply

### HTTP Response

Parameter | Description
--------- | -----------
status | SUCCESS or FAIL
statusCode | 0 of success otherwise a code describing the error
message | null on success, description of error otherwise
organizationId | The organization Id we saved the rate for
sourceCurrency | The source currency this rate applies to
destinationCurrency | The destination currency this rate applies to
rate | The FX rate

## Delete rate for organization

> Request example

```json
{
	"organizationId":"9b53803f-1818-466b-b6bd-393fcc33e67a",
	"sourceCurrency":"EUR",
	"destinationCurrency":"GHS"
}
```

> Response example

```json
{
    "statusCode": 0,
    "message": null,
    "status": "SUCCESS",
}
```

This endpoint is used by an organization to delete a specific FX rate for a currency exchange.

### HTTP Request

`POST https://example.com/price/deleteOrganizationRate`

### Request Parameters

Parameter | Description
--------- | -----------
organizationId | The organization Id we want to delete the rate for
sourceCurrency | The source currency this rate applies to
destinationCurrency | The destination currency this rate applies to

### HTTP Response

Parameter | Description
--------- | -----------
status | SUCCESS or FAIL
statusCode | 0 of success otherwise a code describing the error
message | null on success, description of error otherwise

## Get all organization rate data

> Request example

```json
{
	"organizationId":"9b53803f-1818-466b-b6bd-393fcc33e67a"
}
```

> Response example

```json
{
    "statusCode": 0,
    "message": null,
    "status": "SUCCESS",
    "organizationId": "9b53803f-1818-466b-b6bd-393fcc33e67a",
    "organizationRatesData": [
        {
            "statusCode": 0,
            "message": null,
            "status": "SUCCESS",
            "organizationId": null,
            "sourceCurrency": "EUR",
            "destinationCurrency": "GHS",
            "rate": "66.274000"
        }
    ]
}
```

This endpoint is used to get all the saved rates for an organization

### HTTP Request

`POST https://example.com/price/getAllOrganizationRates`

### Request Parameters

Parameter | Description
--------- | -----------
organizationId | The organization Id we want to the rate data for


### HTTP Response

Parameter | Description
--------- | -----------
status | SUCCESS or FAIL
statusCode | 0 of success otherwise a code describing the error
message | null on success, description of error otherwise
organizationId | The organization Id we want to the rate data for
organizationRatesData | An array of rate data, see example for details

## Get rate for organization

> Request example

```json
{
	"organizationId":"9b53803f-1818-466b-b6bd-393fcc33e67a",
	"sourceCurrency":"EUR",
	"destinationCurrency":"GHS",
}
```

> Response example

```json
{
    "statusCode": 0,
    "message": null,
    "status": "SUCCESS",
    "organizationId": "9b53803f-1818-466b-b6bd-393fcc33e67a",
    "sourceCurrency": "EUR",
    "destinationCurrency": "GHS",
    "rate": "66.274"
}
```

This endpoint is used by an organization to get a specific FX rate for a currency exchange

### HTTP Request

`POST https://example.com/price/getOrganizationRate`

### Request Parameters

Parameter | Description
--------- | -----------
organizationId | The organization Id we want to get the rate for
sourceCurrency | The source currency this rate applies to
destinationCurrency | The destination currency this rate applies to

### HTTP Response

Parameter | Description
--------- | -----------
status | SUCCESS or FAIL
statusCode | 0 of success otherwise a code describing the error
message | null on success, description of error otherwise
organizationId | The organization Id we saved the rate for
sourceCurrency | The source currency this rate applies to
destinationCurrency | The destination currency this rate applies to
rate | The FX rate

## Price request

> Request example

```json
{
  "amount" : "500",
  "sourceCurrency": "EUR",
  "destinationCurrency" : "INR",
  "fundingSource" : "CASH",
  "sendCountry" : "FR",
  "receiveCountry" : "IN",
  "organizationId" : "9b53803f-1818-466b-b6bd-393fcc33e67a"
}
```

> Response example

```json
{
    "statusCode": 0,
    "message": null,
    "status": "SUCCESS",
    "organizationId": null,
    "amountSentSRC": 500,
    "fxCommissionSRCUSDPercent": 0,
    "fxCommissionUSDDESTPercent": 0,
    "organizationFeePercent": 0,
    "organizationFeeFixed": 0,
    "fxRateSRCUSD": 0.873896,
    "fxRateUSDDEST": 64.730003,
    "fxRateSRCUSDWithCommission": 0.873896,
    "fxRateUSDDESTWithCommission": 64.730003,
    "amountSentInUSD": 572.150462,
    "organizationFeeAmountInUSD": 0,
    "organizationFeeFXSRCInUSD": 0,
    "organizationFeeFXDESTInUSD": 0,
    "reconciliationAmountUSD": 0.004652,
    "amountReceivedUSD": 572.14581,
    "amountReceivedDEST": 37035,
    "organizationRevenueUSD": 0.004652,
    "organizationTotalFeePercent": 0.000008,
    "fsCostFixedUSD": 0,
    "fsCostPercent": 0
}
```

This endpoint is used to calculate the price of a transaction, the current rates are fetched in addition to the organization price data and the received amount is calculated.
The amount the recipient gets is returned in the `amountReceivedDEST` field.

### HTTP Request

`POST https://example.com/price/getPrice`

### Request parameters

Parameter | Description
--------- | -----------
amount | The amount sent denominated in source currency
sourceCurrency | The currency the money is sent in
destinationCurrency | The currency the receiver will get the money in
fundingSource | The funding source of the transaction, can be `CASH` or `CREDIT_CARD`
sendCountry | The transaction origination country
receiveCountry | The transaction destination country
organizationId | The ID of the organization the transaction is made in

### HTTP Response

Parameter | Description
--------- | -----------
status | SUCCESS or FAIL
statusCode | 0 of success otherwise a code describing the error
message | null on success, description of error otherwise
amountSentSRC | The amount sent denominated in source currency
fxCommissionSRCUSDPercent | The FX commission on the SRC to USD conversion
fxCommissionUSDDESTPercent | The FX commission on the USD to DEST conversion
organizationFeePercent | The transaction fee in percent
organizationFeeFixed | The fixed transaction fee
fxRateSRCUSD | The FX rate between the SRC and USD currencies
fxRateUSDDEST | The FX rate between the USD and DEST currencies
fxRateSRCUSDWithCommission | The FX rate between the SRC and USD currencies including the commission
fxRateUSDDESTWithCommission | The FX rate between the USD and DEST currencies including the commission
amountSentInUSD | The amount sent in USD
organizationFeeAmountInUSD | The organization fee amount in USD
organizationFeeFXSRCInUSD | The organization fee amount on the FX conversion between SRC and USD
organizationFeeFXDESTInUSD | The organization fee amount on the FX conversion between USD and DEST
reconciliationAmountUSD | The amount deducted from the amount sent in USD in order to round the destination amount
amountReceivedUSD | The amount received in USD
amountReceivedDEST | The amount received in DEST currency
organizationRevenueUSD | The total organization revenue of the transaction
organizationTotalFeePercent | The organization total fee in percent
fsCostFixedUSD | The organization fee on the funding source in fixed USD
fsCostPercent | The organization fee on the funding source in percent

## Find user

> Request example

```json
{
	"queryType":"PHONE",
	"query":"+972547111333"
}
```

> Response example

```json
{
    "statusCode": 0,
    "status": "SUCCESS",
    "query": "+972547111333",
    "queryType": "PHONE",
    "result": {
        "statusCode": 0,
        "status": "SUCCESS",
        "email": "",
        "firstName": "Test",
        "lastName": "Test",
        "phoneNumber": "+972547111333",
        "country": "IL",
        "city": "Jer",
        "address": "Salem",
        "password": null,
        "encryptedPassword": null,
        "userId": "afd5e732-85c8-4cac-b10e-7519e5061700",
        "dateOfBirth": "2005-01-01",
        "verificationCode": null,
        "referralCode": null,
        "idNumber": "Ddd:frt",
        "idType": "PASSPORT",
        "idExpiryDate": null,
        "roles": null,
        "accounts": [
            {
                "accountType": "REM_SENDER",
                "connectedEntityId": "afd5e732-85c8-4cac-b10e-7519e5061700"
            }
        ],
    },

}
```

This endpoint is used to query user data, you can search for a user by its phone number or ID

### HTTP Request

`POST https://example.com/user/query`

### Request parameters

Parameter | Description
--------- | -----------
queryType | The query type, can be ID or PHONE
query | The query string, in case of ID then UUID is expected. In case of PHONE, a phone number formatted to E164 standard is excepted

### HTTP Response

Parameter | Description
--------- | -----------
status | SUCCESS or FAIL
statusCode | 0 of success otherwise a code describing the error
message | null on success, description of error otherwise
query | The query from the request
queryType | the queryType from the request
result | An object containing the user data, see below for details.

### User object

Parameter | Description
--------- | -----------
phoneNumber | The users phone number formatted in E164 format.
middleName | The users middle name
firstName | The users first name
lastName | The users last name
country | The users country in 2 letter ISO code, e.g. IN = India, PH = Philippines
dateOfBirth | The users date of birth in 'yyyy-MM-dd' format
city | The user's city (optional)
address | The user's address (optional)
zipCode | The user's zip code
customerCardNumber | The user's customer card number
idNumber | The user's ID number (optional)
idType | The user's ID type, can be PASSPORT or ID (optional)
accounts | The list of the user's accounts

## Find user for organization

> Request example

```json
{
	"queryType":"PHONE",
	"query":"+972547000999",
	"organizationId":"a511a378-ce96-4de1-b7b9-c2e9f180cd99"
}
```

> Response example

```json
{
    "statusCode": 0,
    "message": null,
    "status": "SUCCESS",
    "organizationId": "a511a378-ce96-4de1-b7b9-c2e9f180cd99",
    "query": "+972547000999",
    "queryType": "PHONE",
    "result": {
        "statusCode": 0,
        "message": null,
        "status": "SUCCESS",
        "organizationId": null,
        "email": "user@email.com",
        "firstName": "Useradam",
        "lastName": "Senderafam",
        "phoneNumber": "+972547000999",
        "country": "IL",
        "city": "Tel Aviv",
        "address": "13 street RD",
        "password": null,
        "encryptedPassword": null,
        "userId": "f4a80862-542e-4b9f-8eb6-995a04e4ae3a",
        "dateOfBirth": "1985-09-22",
        "verificationCode": null,
        "referralCode": null,
        "idNumber": "000000",
        "idType": "WORK_PERMIT",
        "idExpiryDate": null,
        "roles": null,
        "accounts": [
            {
                "accountType": "REM_SENDER",
                "connectedEntityId": "f4a80862-542e-4b9f-8eb6-995a04e4ae3a"
            }
        ],
    },
}

```

This endpoint is used to find a user for an organization, this is to be used when a teller uses the dashboard to find a sender

### HTTP Request

`POST https://example.com/user/queryOrg`

### Request parameters

Parameter | Description
--------- | -----------
queryType | The query type, must be PHONE
query |  a phone number formatted to E164 standard is excepted
organizationId | The organization the user belongs to

### HTTP Response

Parameter | Description
--------- | -----------
status | SUCCESS or FAIL
statusCode | 0 of success otherwise a code describing the error
message | null on success, description of error otherwise
query | The query from the request
queryType | the queryType from the request
result | An object containing the user data, see [Find user](#find-user) for details



## Edit user

> Request example

```json
{
	"userId" : "afd5e732-85c8-4cac-b10e-7519e5061700",
	"firstName" : "new",
  "middleName": "archibald",
	"lastName" : "value",
	"email" : "my@email.com",
	"dateOfBirth" : "1988-06-17",
	"idNumber" : "123456",
	"idType" : "ID",
	"idExpiryDate" : "2020-09-11",
	"city" : "Paris",
	"address" : "13 Rue de rue",
  "zipCode" : "444555",
	"password" : "222222"
}
```

> Response example

```json
{
    "statusCode": 0,
    "message": null,
    "status": "SUCCESS",
    "organizationId": null
}
```

This endpoint is used to edit user data, you are required to always send all mandatory fields.
To change a password send it as part of the request, if password is included it is always changed.

### HTTP Request

`POST https://example.com/user/edit`

### Request parameters

Parameter | Description
--------- | -----------
userId | The ID of the user to edit
firstName | The user's first name
middleName (optional) | The user's middle name
lastName | The user's last name
dateOfBirth | The user's date of birth in 'yyyy-MM-dd' format
email | The user's email (optional)
city | The user's city (optional)
address | The user's address (optional)
zipCode | The user's zip code (optional)
idNumber | The user's ID number (optional)
idType | The user's ID type, can be PASSPORT or ID (optional)
idExpiryDate | The users ID expiry date (optional)
password | The user's password (optional)

### Http Response

Parameter | Description
--------- | -----------
status | SUCCESS or FAIL
statusCode | 0 of success otherwise a code describing the error
message | null on success, description of error otherwise

## Add customer card number

> Request example

```json
{
	"customerCardNumber":"000011",
	"userId":"e45b2095-4c72-45a0-827f-c9d80335d301",
	"organizationId":"9b53803f-1818-466b-b6bd-393fcc33e67a"
}
```

> Response example

```json
{
    "statusCode": 0,
    "message": null,
    "status": "SUCCESS",
}
```
This endpoint is used to add a customer card number for a user on organizations that support this field. Card number must be unique across the organization users

### HTTP Request

`POST https://example.com/user/addCustomerCard`

### Request parameters

Parameter | Description
--------- | -----------
userId | The ID of the user to add the card number to
customerCardNumber | The card number
organizationId | The organization this user belongs to

### Http Response

Parameter | Description
--------- | -----------
status | SUCCESS or FAIL
statusCode | 0 of success otherwise a code describing the error
message | null on success, description of error otherwise

## Save recipient

> Request example for Indian recipient

```json
{  
  "senderId":"19c1513d-1d57-4caa-bd8d-ffd3ff70cb90",
  "firstName":"Robute",
  "lastName":"Guliiam",
  "phoneNumber":"+919812349901",
  "country":"IN",
  "dateOfBirth":"1985-07-22",
  "ifscCode":"ICIC000321",
  "accountNumber":"101267",
  "bankCode": "9100041",
}

```

> Request example for other recipients

```json
{  
  "senderId":"19c1513d-1d57-4caa-bd8d-ffd3ff70cb90",
  "firstName":"Robute",
  "lastName":"Guliiam",
  "phoneNumber":"+63881234991",
  "country":"PH",
  "dateOfBirth":"1985-07-22",
  "accountNumber":"101267",
  "bankCode": "9100041",
  "bankName": "my bank",
  "bankBranchName": "my branch",
  "bankBranchCode": "12223",
  "bankAddress": "131 my street",
  "bankCity": "mycity",
  "ezwitchNumber":"1234123512"
}

```

> Response example

```json
{
    "statusCode": 0,
    "message": null,
    "status": "SUCCESS",
    "organizationId": null,
    "recipientId": "4b8d56a7-caa6-41e4-9650-14b44b5ddf65",
    "senderId": "19c1513d-1d57-4caa-bd8d-ffd3ff70cb90",
    "firstName": "Robute",
    "lastName": "Guliiam",
    "phoneNumber": "+63881234991",
    "country": "PH",
    "dateOfBirth": "1985-07-22",
    "ifscCode": null,
    "accountNumber": "101267",
    "bankName": "my bank",
    "bankBranchName": "my branch",
    "bankBranchCode": "12223",
    "bankAddress": "131 my street",
    "bankCity": "mycity",
    "bankDistrict": null,
    "bankState": null,
    "bankCode": "9100041",
    "ezwitchNumber":"1234123512"
}

```

This endpoint is used to add recipients.
In case the recipient is from India and has a bank account only accountNumber, ifscCode and bankCode are required.
To get the bank codes please call the [Get bank names](#get-bank-names) method

### HTTP request

`POST https://example.com/recipient/add`

### Request parameters

Parameter | Description
--------- | -----------
senderId | The sender to add this recipient to
firstName | The recipient's first name
lastName | The recipient's last name
country | The recipient's country in 2 letter ISO code, e.g. IN = India, PH = Philippines
dateOfBirth | The recipient's date of birth
ifscCode (optional) | The recipients IFSC code (if relevant)
accountNumber (optional) | The recipients bank account number
bankName (optional) | The bank name
bankBranchName (optional) | The bank branch name
bankBranchCode (optional) | The bank branch code
bankAddress (optional)| The bank address
bankCity (optional)| The bank city
bankCode (optional) | The code representing the selected bank
ezwitchNumber (optional) | The E-zwitch card number of the recipient

### HTTP response

The response contains object given to the server including the new ID assigned to the recipient.
In addition the message status and status code are returned, please review the response example for details.

## Edit recipient

> Request example

```json
{  
  "recipientId":"4b8d56a7-caa6-41e4-9650-14b44b5ddf65",
  "senderId":"19c1513d-1d57-4caa-bd8d-ffd3ff70cb90",
  "firstName":"Robutv",
  "lastName":"Guliiam",
  "phoneNumber":"+63881234991",
  "country":"PH",
  "dateOfBirth":"1985-07-22",
  "accountNumber":"101267",
  "bankCode": "9100041",
  "bankName": "my bank",
  "bankBranchName": "my branch",
  "bankBranchCode": "12223",
  "bankAddress": "131 my street",
  "bankCity": "mycity",
  "ezwitchNumber":"1234123512"
}
```

> Response example

```json
{
    "statusCode": 0,
    "message": null,
    "status": "SUCCESS",
    "organizationId": null,
    "recipientId": "4b8d56a7-caa6-41e4-9650-14b44b5ddf65",
    "senderId": "19c1513d-1d57-4caa-bd8d-ffd3ff70cb90",
    "firstName": "Robutv",
    "lastName": "Guliiam",
    "phoneNumber": "+63881234991",
    "country": "PH",
    "dateOfBirth": "1985-07-22",
    "ifscCode": null,
    "accountNumber": "101267",
    "bankName": "my bank",
    "bankBranchName": "my branch",
    "bankBranchCode": "12223",
    "bankAddress": "131 my street",
    "bankCity": "mycity",
    "bankDistrict": null,
    "bankState": null,
    "bankCode": "9100041",
    "ezwitchNumber":"1234123512"
}
```

This end point is used to edit a recipient details, the request and response parameters are the same the [save recipient](#save-recipient) method, please follow the same guidelines. The only difference is that the request here includes the recipient id to edit.

### HTTP request

`POST https://example.com/recipient/edit`

## Find recipient

> Request example

```json
{  
  "senderId":"e73ceaf0-36cf-4602-b1b9-e06d120e6e61"
}
```

> Response example

```json
{
  "recipients": [
      {
          "statusCode": 0,
          "message": null,
          "status": "SUCCESS",
          "organizationId": null,
          "recipientId": "ddd4b9b6-e1a3-4cf2-a4f0-8b941aa046dc",
          "senderId": null,
          "firstName": "rajann",
          "lastName": "prakash",
          "phoneNumber": "+919123456780",
          "country": "IN",
          "dateOfBirth": "1985-09-22",
          "ifscCode": "icic000999",
          "accountNumber": null,
          "bankName": "fakeyBakey",
          "bankBranchName": null,
          "bankAddress": null,
          "bankCity": null,
          "bankDistrict": null,
          "bankState": null
      }
  ]
}
```

This endpoint returns an array with all the given users recipients

### HTTP Request

`POST https://example.com/recipient/find`

### Request parameters

Parameter | Description
--------- | -----------
senderId | The user ID to get the recipients for

### HTTP Response

Parameter | Description
--------- | -----------
recipients | An array of Recipients (see below)

### Recipient Object

Parameter | Description
--------- | -----------
recipientId | The ID of this recipient
phoneNumber | The users phone number formatted in E164 format.
firstName | The users first name
lastName | The users last name
country | The users country in 2 letter ISO code, e.g. IN = India, PH = Philippines
dateOfBirth | The users date of birth
ifscCode (optional) | The recipients IFSC code (if relevant)
accountNumber (optional) | The recipients bank account number
bankName (optional) | The bank name
bankBranchName (optional)| The bank branch name
bankAddress (optional)| The bank address
bankCity (optional)| The bank city
bankDistrict (optional)| The bank district
bankState (optional)| The bank district
bankCode (optional) | The bank code, used by some of the operators when doing a bank transaction

## Upload image

This endpoint is used to upload images to server, the endpoint expects a request in form-data format where
the key is the user ID with a png file extension (example: 68e1dd6a-624d-4f34-b109-acf116fa2ffc.png) and the value
is the file.
This endpoint is to be used to upload user passport or ID images

### HTTP request

`POST https://example.com/profilePicture/put`

### HTTP response

Parameter | Description
--------- | -----------
status | SUCCESS or FAIL
statusCode | 0 of success otherwise a code describing the error
message | null on success, description of error otherwise

<aside class="notice">
The method expects png files.
</aside>

## Download images

This end point allows you to download images from the server.

### HTTP request

`GET https://example.com/profilePicture/get?id=xxx`

### Request parameters

Parameter | Description
-------|-------
id | The user id we are requesting the image for.
base64 | Get the image in base 64 text format

<aside class="notice">
The method always returns png files if not choosing base64 format.
</aside>

### HTTP response

The requested image

## Get bank details by IFSC

> Request example

```json
{
  "ifscCode":"IBKL0000387"
}
```

> Response example

```json
{
    "statusCode": 0,
    "message": null,
    "status": "SUCCESS",
    "organizationId": null,
    "bankName": "IDBI BANK",
    "bankAddress": "IDBI BANK LTD  SPECIALISED CORPORATE BRANCH  SCO 126 128  1ST FLOOR  KALINGA TOWERS FEROZE GANDHI MARKET  LUDHIANA 141001",
    "bankCity": "LUDHIANA",
    "bankState": "PUNJAB",
    "bankDistrict": "LUDHIANA",
    "bankBranchName": "SPECIALISED CORPORATE BRANCH  LUDHIANA ",
    "ifscCode": "IBKL0000387"
}

```

This endpoint returns bank details according to the given IFSC code

### HTTP Request

`POST https://example.com/transaction/queryIFSC`

### Request Parameters

Parameter | Description
--------- | ----------
ifscCode | The IFSC code to query


### HTTP Response

Parameter | Description
--------- | ----------
status | SUCCESS or FAIL
statusCode | 0 of success otherwise a code describing the error
message | null on success, description of error otherwise
bankName | The bank name
bankBranchName | The bank branch name
bankAddress | The bank address
bankCity | The bank city
bankDistrict | The bank district
bankState | The bank district
ifscCode | The code used in this query

## Get bank names

> Request example

```json
{
	"operatorId": "6d62d31c-1809-489e-9ac2-0889ac53d17f",
	"country" : "PH"
}
```

> Response example

```json
{
    "statusCode": 0,
    "message": null,
    "status": "SUCCESS",
    "organizationId": null,
    "operatorId": "6d62d31c-1809-489e-9ac2-0889ac53d17f",
    "country": "PH",
    "bankDetails": [
        {
            "bankName": "BDO Consl",
            "bankCode": "0001"
        }
    ]
}
```

This endpoint returns a message containing a list of the bank names we support transfer to for a specific operator and country

### HTTP Request

`POST https://example.com/operator/getBanksByCountry`

### Query Parameters

Parameter | Description
--------- | -----------
operatorId | The ID of the operator
country | The country to search for banks. Country should be supplied 2 letter ISO code (e.g. IN = India)

### HTTP response

Parameter | Description
--------- | -----------
status | SUCCESS or FAIL
statusCode | 0 of success otherwise a code describing the error
message | null on success, description of error otherwise
operatorId | The ID of the operator
country | The country to search for banks. Country should be supplied 2 letter ISO code (e.g. IN = India)
bankDetails | An array of bank details objects (see below)

### BankDetails object

Parameter | Description
--------- | -----------
bankName | The banks name to display
bankCode | the code identifying this bank for the operator

## Get Bank Branches

> Request Example

```json
{
  "operatorId": "7508343d-56b0-47c3-8ddb-86a7e89988e3",
  "country" : "GH",
  "bankCode": "20"
}
```

> Response Example

```json
{
    "operatorId": "7508343d-56b0-47c3-8ddb-86a7e89988e3",
    "country": "GH",
    "bankCode": "20",
    "bankBranchDetailsDetails": [
        {
            "branchName": "ABII NATIONAL SAVINGS AND LOANS",
            "branchCode": "0000210900"
        },
        {
            "branchName": "ABOSSEY OKAI",
            "branchCode": "0000210119"
        }
    ]
}
```

### HTTP Request

`POST https://example.com/operator/getBankBranchesByCountry`

### Query Parameters

Parameter | Description
--------- | -----------
operatorId | The ID of the operator
country | The country to search for banks. Country should be supplied 2 letter ISO code (e.g. IN = India)
bankCode | The code of the bank we are searching the branches for

### HTTP response

Parameter | Description
--------- | -----------
status | SUCCESS or FAIL
statusCode | 0 of success otherwise a code describing the error
message | null on success, description of error otherwise
operatorId | The ID of the operator
country | The country to search for banks. Country should be supplied 2 letter ISO code (e.g. IN = India)
bankCode | The code of the bank we are searching the branches for
bankBranchDetailsDetails | An array of bank branch details objects (see below)

### BankBranchDetails object

Parameter | Description
--------- | -----------
branchName | The branch name to display
branchCode | the code identifying this branch for the operator

## Get Mobile operators by country

> Request example

```json
{
	"operatorId":"7508343d-56b0-47c3-8ddb-86a7e89988e3",
	"country":"GH"
}
```

> Response example

```json
{
    "statusCode": 0,
    "message": null,
    "status": "SUCCESS",
    "organizationId": null,
    "operatorId": "7508343d-56b0-47c3-8ddb-86a7e89988e3",
    "country": "GH",
    "mobileOperatorDetails": [
        {
            "mobileOperatorName": "INFENIX"
        },
        {
            "mobileOperatorName": "MTN"
        }
    ]
}

```

This end point returns a list of all the mobile operators supported by the given organization in the given country

### HTTP Request

`POST https://example.com/operator/getMobileOperatorsByCountry`

### Query Parameters

Parameter | Description
--------- | -----------
operatorId | The ID of the operator
country | The country to search for mobile operators in. Country should be supplied 2 letter ISO code (e.g. IN = India)

### HTTP response

Parameter | Description
--------- | -----------
status | SUCCESS or FAIL
statusCode | 0 of success otherwise a code describing the error
message | null on success, description of error otherwise
operatorId | The ID of the operator
country | The country to search for mobile operators. Country should be supplied 2 letter ISO code (e.g. IN = India)
mobileOperatorDetails | An array of mobile operator details objects (see below)

### MobileOperatorDetails object

Parameter | Description
--------- | -----------
mobileOperatorName | The location name to display


## Get cash pickup location names

> Request example

```json
{
	"operatorId": "6d62d31c-1809-489e-9ac2-0889ac53d17f",
	"country" : "PH"
}
```

> Response example

```json
{
    "statusCode": 0,
    "message": null,
    "status": "SUCCESS",
    "organizationId": null,
    "operatorId": "6d62d31c-1809-489e-9ac2-0889ac53d17f",
    "country": "PH",
    "locationDetails": [
        {
            "locationName": "BDO",
            "locationCode": "1955"
        }
    ]
}
```

This endpoint returns a message containing a list of business names we support cash pickup from for a specific operator and country

### HTTP Request

`POST https://example.com/operator/getCashPickupByCountry`

### Query Parameters

Parameter | Description
--------- | -----------
operatorId | The ID of the operator
country | The country to search for banks. Country should be supplied 2 letter ISO code (e.g. IN = India)

### HTTP response

Parameter | Description
--------- | -----------
status | SUCCESS or FAIL
statusCode | 0 of success otherwise a code describing the error
message | null on success, description of error otherwise
operatorId | The ID of the operator
country | The country to search for banks. Country should be supplied 2 letter ISO code (e.g. IN = India)
locationDetails | An array of location details objects (see below)

### LocationDetails object

Parameter | Description
--------- | -----------
locationName | The location name to display
locationCode | the code identifying this location for the operator

## Get Operator balance

> Request example

```json
{
	"operatorId":"387d826d-a6ce-4a23-b178-d71729e6387d",
	"organizationId":"9b53803f-1818-466b-b6bd-393fcc33e67a"
}
```

> Response example

```json
{
    "statusCode": 0,
    "message": null,
    "status": "SUCCESS",
    "organizationId": "9b53803f-1818-466b-b6bd-393fcc33e67a",
    "operatorId": "387d826d-a6ce-4a23-b178-d71729e6387d",
    "balance": "99994790.00",
    "currency": "USD"
}
```

This endpoint returns the current balance a give organization has at a given operatorId

### HTTP request

`POST https://example.com/operator/getBalance`

### Query Parameters

Parameter | Description
--------- | -----------
operatorId | The ID of the operator
organizationId | The ID of the organizationId

### HTTP response

Parameter | Description
--------- | -----------
status | SUCCESS or FAIL
statusCode | 0 of success otherwise a code describing the error
message | null on success, description of error otherwise
operatorId | The ID of the operator
organizationId | The ID of the organizationId
balance | The current balance if the service is not available for this operator the value will be -1
currency | The balance currency value
