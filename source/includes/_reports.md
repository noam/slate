# Reports

Below are all endpoints used in the various reports in the system

## Teller most recent transactions report

> Example for a request:

```json
{
	"userId":"afd5e732-85c8-4cac-b10e-7519e5061700",
	"agentId":"3ac7f3f1-3e5e-48b1-8173-2c6bc62475f2",
	"pageNum":"0"
}
```

> Response example:

```json
{
    "statusCode": 0,
    "message": null,
    "status": "SUCCESS",
    "organizationId": null,
    "agentId": "3ac7f3f1-3e5e-48b1-8173-2c6bc62475f2",
    "userId": "afd5e732-85c8-4cac-b10e-7519e5061700",
    "pageNum": 0,
    "totalPages": 1,
    "reportResult": [
        {
            "transactionId": "585e323a-61c8-4e91-8d66-44f6ab8aed66",
            "date": "2017-07-11 16:30:30.814",
            "senderName": "Sebastian Yarik",
            "receiverName": "Pek Ling Lau",
            "amountSent": "700.00",
            "amountReceived": "40478",
            "sourceCurrency": "EUR",
            "destinationCurrency": "PHP",
            "depositCode": "0077983078",
            "reservationCode": "5077985519",
            "payoutType": "BANK_ACCOUNT",
            "transactionStatus": "PROCESSING"
        }
    ],
}
```
This endpoint is used to get the teller's most recent transactions.
This request will return a page with the 25 most recent transactions of the teller.
It is possible to request additional pages that contain more transactions.
If no transactions are found for the request a HTTP 404 status is returned


### HTTP Request

`POST https://example.com/transactionNew/agentRecentTransactions`

### Query Parameters

Parameter | Description
--------- | -----------
userId | The transactions returned will be the transactions confirmed by this user id
agentId | The transactions returned will be the transactions confirmed at this agent
pageNum | The page to request

### HTTP response

Parameter | Description
--------- | -----------
status | SUCCESS or FAIL
statusCode | 0 of success otherwise a code describing the error
message | null on success, description of error otherwise
userId | The transactions returned will be the transactions confirmed by this user id
agentId | The transactions returned will be the transactions confirmed at this agent
pageNum | The page returned
totalPages | The total available pages for this report
reportResult | A list of transaction details (see below)

### Transaction report details object

Parameter | Description
--------- | -----------
transactionId | The transactions UUID
date | tHe transaction date, given in `yyyy-MM-dd HH:mm.zzz` format
senderName | The sender name
receiverName | The recipients name
transactionStatus | The transaction status
depositCode | The transaction deposit code
reservationCode | The transaction reservation code
amountSent | The transaction amount in source currency
amountReceived | The transaction amount in destination currency
sourceCurrency | The source currency
destinationCurrency | The destination currency
payoutType | The transaction payout type, can be `CASH` or `BANK_ACCOUNT`

## User most recent transactions report

> Request example

```json
{
	"userId":"afd5e732-85c8-4cac-b10e-7519e5061700",
	"organizationId":"9b53803f-1818-466b-b6bd-393fcc33e67a"
}
```

> Response example

```json
{
    "statusCode": 0,
    "message": null,
    "status": "SUCCESS",
    "organizationId": null,
    "agentId": null,
    "userId": "19c1513d-1d57-4caa-bd8d-ffd3ff70cb90",
    "amount": null,
    "numTransactions": 0,
    "revenue": null,
    "pageNum": 0,
    "totalPages": 0,
    "startDateMillis": 0,
    "endDateMillis": 0,
    "reportResult": [
        {
            "transactionId": "20e081b0-2202-42c5-8481-0bde969b3bdd",
            "date": "2017-06-04 13:40:24.914",
            "senderName": "Sebastian Yarik",
            "receiverName": "Robute Guliiam",
            "phoneNumber": null,
            "amountSent": "700.00",
            "amountReceived": "51582",
            "sourceCurrency": "EUR",
            "destinationCurrency": "INR",
            "depositCode": "5473282489",
            "reservationCode": "5273288858",
            "payoutType": "BANK_ACCOUNT",
            "transactionStatus": "COMPLETE_SUCCESS"
        }
		]
}
```

This endpoint is used to get the 10 most recent transactions for a user.
If no transactions are found for the request a HTTP 404 status is returned.

### HTTP Request

`POST https://example.com/transactionNew/senderRecentTransactions`

### Query Parameters

Parameter | Description
--------- | -----------
userId | The transactions returned will be the transactions sent by this user id
organizationId | The organization id the user belongs to

### HTTP response

Parameter | Description
--------- | -----------
status | SUCCESS or FAIL
statusCode | 0 of success otherwise a code describing the error
message | null on success, description of error otherwise
userId | The transactions returned will be the transactions confirmed by this user id
agentId | The transactions returned will be the transactions confirmed at this agent
reportResult | A list of transaction details [see Report Data object](#transaction-report-details-object)

## Business overview report

> Request Example

```json
{
	"organizationId":"9b53803f-1818-466b-b6bd-393fcc33e67a"
}
```

> Response example

```json
{
    "statusCode": 0,
    "message": null,
    "status": "SUCCESS",
    "organizationId": "9b53803f-1818-466b-b6bd-393fcc33e67a",
    "totalTurnOver": 1597,
    "totalTurnoverLastMonth": 1098,
    "turnoverDifferencePercent": 0.4544626593806922,
    "revenue": 2000,
    "revenueLastMonth": 1500,
    "revenueDifferencePercent": 0.333333333,
    "profit": 1000,
    "profitLastMonth": 750,
    "profitDifferencePercent": 0.25,
    "totalTransactions": 2,
    "totalTransactionsLastMonth": 2,
    "totalTransactionDifferencePercent": 0,
    "usersThisMonth": 0,
    "usersLastMonth": 4,
    "usersDifferencePercent": -1,
    "countryTransactions": {
        "FR": 2
    },
		"turnOverReportData": [
        {
            "turnover": "799",
            "date": "2017-06-01 00:00:00.0"
        },
        {
            "turnover": "1098",
            "date": "2017-07-01 00:00:00.0"
        }
    ]
}

```

This endpoint returns the data for the business overview report

### HTTP Request

`POST https://example.com/report/businessOverview`

### Query Parameters

Parameter | Description
--------- | -----------
organizationId | The organization id for the report

### HTTP Response

Parameter | Description
--------- | -----------
status | SUCCESS or FAIL
statusCode | 0 of success otherwise a code describing the error
message | null on success, description of error otherwise
totalTurnOver | The total turnover for this month
totalTurnoverLastMonth | The total turnover for last month
turnoverDifferencePercent | The difference in percent for the two values
revenue | This month revenue
revenueLastMonth | Last month revenue
revenueDifferencePercent | the difference in revenue in percent
profit | This month profit
profitLastMonth | Last month profit
profitDifferencePercent | The difference in profit in percent
totalTransactions | Total transactions this month
totalTransactionsLastMonth | Last month total transactions
totalTransactionDifferencePercent | The difference in the number of transactions in percent
usersThisMonth | Number of new users this month
usersLastMonth | Number of new users last month
usersDifferencePercent | Difference in the number of new users in percent
countryTransactions | The number of transactions for the top 3 performing countries
turnOverReportData | List graph data, see below

### Graph data object

Parameter | Description
--------- | -----------
date | The date of the current point
turnover | The turnover in USD for the current point

## Transactions report

> Request example

```json
{
	"organizationId":"9b53803f-1818-466b-b6bd-393fcc33e67a",
	"pageNum":"0",
	"sortField":"RECIEVER",
	"fullResult":"false",
	"sortDirection":"ASC",
	"senderName":"se"
}
```

> Response example

```json
{
    "statusCode": 0,
    "message": null,
    "status": "SUCCESS",
    "organizationId": "9b53803f-1818-466b-b6bd-393fcc33e67a",
    "pageNum": 0,
    "totalPages": 1,
    "totalRows": 6,
    "fullResult": false,
    "sortDirection": "ASC",
    "sortField": "RECIEVER",
    "startDate": null,
    "endDate": null,
    "depositCode": null,
    "senderName": "Se",
    "destinationCountry": null,
    "deliveryMethod": null,
    "transactionStatus": null,
    "reportData": [
        {
            "date": "2017-07-16 15:08:51.252",
            "code": "1820693124",
            "sender": "Test Sender",
            "receiver": "Frank Oppo",
            "amountSent": "300.00",
            "amountReceived": "15158",
            "status": "FAIL",
            "sendCurrency": "USD",
            "receiveCurrency": "PHP"
        }
    ]
}
```

This endpoint is used to get results for the transaction report.

Results are paged with page size of 25, user can specify if he wants all the rows by passing `true` in the `fullResult` parameter.

The user may specify search parameters (see below), if more than one is supplied the result will be a logical AND of the parameters.

### HTTP Request

`POST https://example.com/report/transactionReport`

### Query Parameters

Parameter | Description
--------- | -----------
organizationId | The organization id for the report
pageNum | The number of the page requested, pages start at 0
fullResult | Wether to return all the rows or a page of 25 results, valid values are `true` and `false`
sortDirection | The sort direction, can be `ASC` or `DESC`
sortField | The field on which to sort the results, values are `DATE, CODE, SENDER, RECIEVER, AMOUNT_SENT, AMOUNT_RECEIVED, STATUS`
startDate (optional) | If specified will return results of transactions created between the given start date and end date. If start date is specified end date must be specified as well. Expected format is `yyyy-[m]m-[d]d hh:mm:ss`
endDate (optional) | If specified will return results of transactions created between the given start date and end date. If end date is specified start date must be specified as well. Expected format is `yyyy-[m]m-[d]d hh:mm:ss`
depositCode (optional) | If specified will return transactions where the deposit code is like the given parameter
senderName (optional) | If specified will return transactions where the sender name matches the given query, case insensitive
destinationCountry (optional) | If specified will return transactions where the recipient country matches the given parameter, must be given in 2 letter ISO code.
deliveryMethod (optional) | If specified will return transactions that match the given delivery method, available values are to be taken from the `payoutTypes` parameters on the organization data in the login response
transactionStatus (optional) | If specified will return transactions that their status match the given parameter
senderId (optional) | If specified will return transactions that the sender has the given ID

### HTTP Response

Parameter | Description
--------- | -----------
status | SUCCESS or FAIL
statusCode | 0 of success otherwise a code describing the error
message | null on success, description of error otherwise
pageNum | The current page number
totalPages | The total pages in the report
totalRows | The total number of rows in the report
reportData | A list of report data objects to display (see below)

### Report data object

Parameter | Description
--------- | -----------
date | The transaction creation date
code | The deposit code
sender | The sender's name
receiver | The receiver's name
amountSent | The amount sent in source currency
amountReceived | The amount received in destination currency
status | The transaction status
sendCurrency | The source currency
receiveCurrency | The destination currency

## Customers report

> Request example

```json
{
	"organizationId":"9b53803f-1818-466b-b6bd-393fcc33e67a",
	"pageNum" : 0,
	"fullResult": false,
	"customerSortField":"FIRST_NAME",
	"sortDirection":"DESC"
}
```

> Response example

```json
{
    "statusCode": 0,
    "message": null,
    "status": "SUCCESS",
    "organizationId": "9b53803f-1818-466b-b6bd-393fcc33e67a",
    "pageNum": 0,
    "totalPages": 1,
    "totalRows": 12,
    "fullResult": false,
    "sortDirection": "DESC",
    "customerSortField": "FIRST_NAME",
    "name": null,
    "phoneNumber": null,
    "idNumber": null,
    "destCountry": null,
    "activeStartDate": null,
    "activeEndDate": null,
    "reportData": [
        {
            "firstName": "Za",
            "lastName": "evilgen",
            "idNumber": "123456",
            "phoneNumber": "+972546222333",
            "recentTransaction": "N/A",
            "appUser": false
        }
		]
}
```

This endpoint is used to get results for the customer report.

Results are paged with page size of 25, user can specify if he wants all the rows by passing `true` in the `fullResult` parameter.

The user may specify search parameters (see below), if more than one is supplied the result will be a logical AND of the parameters.

### HTTP Request

`POST https://example.com/report/customerReport`

### Query Parameters

Parameter | Description
--------- | -----------
organizationId | The organization id for the report
pageNum | The number of the page requested, pages start at 0
fullResult | Wether to return all the rows or a page of 25 results, valid values are `true` and `false`
sortDirection | The sort direction, can be `ASC` or `DESC`
sortField | The field on which to sort the results, values are `FIRST_NAME, LAST_NAME, ID_NUMBER, PHONE_NUMBER, RECENT_TRANSACTION, APP_USER`
name (optional) | If specified will find customers with name matching the given parameter, case insensitive
phoneNumber (optional) | If specified will find customers with phone number matching the given parameter
idNumber (optional) | If specified will find customers with ID number matching the given parameter
isAppUser (optional) | If specified will find customers that signed up via the app
activeStartDate (optional) | If specified will return results of customers with transactions created between the given start date and end date. If start date is specified end date must be specified as well. Expected format is `yyyy-[m]m-[d]d hh:mm:ss`
activeEndDate (optional) | If specified will return results of customers with transactions created between the given start date and end date. If end date is specified start date must be specified as well. Expected format is `yyyy-[m]m-[d]d hh:mm:ss`

### HTTP Response

Parameter | Description
--------- | -----------
status | SUCCESS or FAIL
statusCode | 0 of success otherwise a code describing the error
message | null on success, description of error otherwise
pageNum | The current page number
totalPages | The total pages in the report
totalRows | The total number of rows in the report
reportData | A list of report data objects to display (see below)

### Report data object

Parameter | Description
--------- | -----------
firstName | The customers first name
lastName | The customers last name
idNumber | The customers ID number
phoneNumber | The customers phone number
recentTransaction | The date of the most recent transaction, `N/A` if no transactions
appUser | `true` if customer signed up in the app, `false` otherwise

## Operator report

> Request example

```json
{
	"organizationId":"a511a378-ce96-4de1-b7b9-c2e9f180cd99"
}
```

> Response example

```json
{
    "statusCode": 0,
    "message": null,
    "status": "SUCCESS",
    "organizationId": "a511a378-ce96-4de1-b7b9-c2e9f180cd99",
    "data": [
        {
            "operatorId": "78f35217-d86c-469e-a06c-e77c39df2743",
            "operatorName": "Muthoot",
            "email": "support@muthoot.com",
            "phone": "+919123456789",
            "numTransactions": 2,
            "difference": 1
        }
    ]
}
```

This endpoint is used to get the operator report data, not including the balance.


### HTTP Request

`POST https://example.com/report/operatorReport`

### Query Parameters

Parameter | Description
--------- | -----------
organizationId | The organization id for the report

### HTTP Response

Parameter | Description
--------- | -----------
organizationId | The organization id for the report
status | SUCCESS or FAIL
statusCode | 0 of success otherwise a code describing the error
message | null on success, description of error otherwise
data | An array of operator data (see below)

### Operator data object

Parameter | Description
--------- | -----------
operatorId | The operator ID
operatorName | The name of the operator
email | The operator's support email
phone | The operator's support phone number
numTransactions | Number of transactions this month for this operator
difference | difference in number of transactions from last month in percent (1 = 100%)
