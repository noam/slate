# Agents and Branches

The endpoints below deal with agents and branches, to make thing clearer please review the terminology below:

**Organization** - A money transfer business, may have one or more agents.

**Agent** - A physical branch of the organization, employs tellers and acts as a place for end users to deposit cash and facilitate transactions.

**Teller** - An employee of the organization working at a specific agent, these are the users that operate the dashboard.

## Find Agent by id

> Request example

```json
{
	"id":"3ac7f3f1-3e5e-48b1-8173-2c6bc62475f2"
}
```

> Response example

```json
{
    "statusCode": 0,
    "message": null,
    "status": "SUCCESS",
    "organizationId": "9b53803f-1818-466b-b6bd-393fcc33e67a",
    "id": "3ac7f3f1-3e5e-48b1-8173-2c6bc62475f2",
    "name": "Adam Express",
    "address": "108 Levinsky",
    "city": "Tel Aviv",
    "country": "IL",
    "obligoUsd": 0,
    "longitude": null,
    "latitude": null,
    "phoneNumber": null,
    "tellers": [
        {
            "id": "19c1513d-1d57-4caa-bd8d-ffd3ff70cb90",
            "firstName": "Sebastian",
            "lastName": "Yarik",
            "phoneNumber": "+33523111000",
            "email": "seb@arma.com"
        }
    ]
}

```

This endpoint returns data on an agent according to the given ID

### HTTP request

`POST https://example.com/agent/findAgent`

### Request parameters

Parameter | Description
--------- | -----------
id | The ID of the agent to find

### HTTP response

Parameter | Description
--------- | -----------
status | SUCCESS or FAIL
statusCode | 0 of success otherwise a code describing the error
message | null on success, description of error otherwise
id | The agent's id, populate this field only if editing an existing agent
organizationId | The ID of the organization the agent belongs to
name | The agent's name
address | the agent's address
city | The agent's city
country | The agent's country
obligoUsd | The agent's initial obligo (optional)
longitude | The agent's location longitude (optional)
latitude | The agent's location latitude (optional)
phoneNumber | the agent's phone number
tellers | A list of the agent's tellers (see )

## Find Agents of an organization

> Request example

```json
{
	"organizationId":"9b53803f-1818-466b-b6bd-393fcc33e67a"
}
```

> Response example

```json
[
  {
    "organizationId": "9b53803f-1818-466b-b6bd-393fcc33e67a",
    "id": "b42477f3-afe6-429d-83b1-13368c69f49f",
    "name": "Paris Exchange",
    "address": "13 rue de Rue",
    "city": "Paris",
    "country": "FR",
    "obligoUsd": 0,
    "longitude": "2.333618",
    "latitude": "48.853656"
  }
]

```

This endpoint returns a list of all the agents in the given organization.

### HTTP request

`POST  https://example.com/agent/findByOrganization`


### Request parameters

Parameter | Description
--------- | -----------
organizationId | The ID of the organization

### HTTP response

The response contains a list of agent objects, see below:

Parameter | Description
--------- | -----------
organizationId | The ID of the organization
id | The agent ID
name | The agent's name
address | the agent's address
city | The agent's city
country | The agent's country
obligoUsd | The agent's current obligo
longitude | The agent's location longitude
latitude | The agent's location latitude
teller | A list of teller data object (see [Teller Object](#teller-data-object) for details)


### Teller Data object

Parameter | Description
--------- | -----------
id | The teller's user ID
firstName | The teller's first name
lastName | The teller's last name


## Find Agents of an organization - by token

> Response example

```json
[
  {
    "organizationId": "9b53803f-1818-466b-b6bd-393fcc33e67a",
    "id": "b42477f3-afe6-429d-83b1-13368c69f49f",
    "name": "Paris Exchange",
    "address": "13 rue de Rue",
    "city": "Paris",
    "country": "FR",
    "obligoUsd": 0,
    "longitude": "2.333618",
    "latitude": "48.853656"
  }
]

```

This endpoint returns a list of all the agents for the organization the logged in user is part of

### HTTP request

`GET  https://example.com/agent/findByOrganization`

### HTTP response

same as [POST find branches of an organization](#find-agents-of-an-organization)

## Save agent

> Request example

```json
{
	"id":"44f68bbb-ae2c-47bc-8fb1-3fee58b0ab03",
	"name":"Paris exchange 2",
  "address":"13 Rue de Rue",
	"city":"Paris",
  "country":"FR",
  "organizationId":"9b53803f-1818-466b-b6bd-393fcc33e67a",
  "obligoUsd":"0",
  "latitude":"48.854",
  "longitude":"2.3344",
	"phoneNumber":"+33547606333"
}
```

> Response example

```json
{
    "statusCode": 0,
    "message": null,
    "status": "SUCCESS",
    "organizationId": "9b53803f-1818-466b-b6bd-393fcc33e67a",
    "id": "44f68bbb-ae2c-47bc-8fb1-3fee58b0ab03",
    "name": "Paris exchange 2",
    "address": "13 Rue de Rue",
    "city": "Paris",
    "country": "FR",
    "obligoUsd": 0,
    "longitude": "2.3344",
    "latitude": "48.854",
		"phoneNumber":"+33547606333"
}
```

This endpoint saves a new or existing agent, to edit an agent send the agent's ID as part of the request.

### HTTP request

`POST  https://example.com/agent/save`


### Request parameters

Parameter | Description
--------- | -----------
id | The agent's id, populate this field only if editing an existing agent
organizationId | The ID of the organization the agent belongs to
name | The agent's name
address | the agent's address
city | The agent's city
country | The agent's country
obligoUsd | The agent's initial obligo (optional)
longitude | The agent's location longitude (optional)
latitude | The agent's location latitude (optional)
phoneNumber | the agent's phone number

### HTTP response

Parameter | Description
--------- | -----------
status | SUCCESS or FAIL
statusCode | 0 of success otherwise a code describing the error
message | null on success, description of error otherwise
id | The agent's id, populate this field only if editing an existing agent
organizationId | The ID of the organization the agent belongs to
name | The agent's name
address | the agent's address
city | The agent's city
country | The agent's country
obligoUsd | The agent's initial obligo (optional)
longitude | The agent's location longitude (optional)
latitude | The agent's location latitude (optional)
phoneNumber | the agent's phone number

## Find users for agent

> Request example

```json
{
	"id":"3ac7f3f1-3e5e-48b1-8173-2c6bc62475f2"
}
```

> Response example

```json
{
    "statusCode": 0,
    "message": null,
    "status": "SUCCESS",
    "organizationId": null,
    "userData": [
        {
            "firstName": "new",
            "lastName": "value",
            "phone": "+972547111333",
        },
        {
            "firstName": "Sebastian",
            "lastName": "Yarik",
            "phone": "+33523111000",
            "email": "seb@arma.com",
        }
    ]
}
```

This endpoint returns all the users of the given agent.

### HTTP Request

`POST https://example.com/agent/findUsersByAgent`

### Request parameters

Parameter | Description
--------- | -----------
id | The agent's id


### HTTP response

Parameter | Description
--------- | -----------
status | SUCCESS or FAIL
statusCode | 0 of success otherwise a code describing the error
message | null on success, description of error otherwise
userData | A list of all the users returned for this agent (see below)

### User data object

Parameter | Description
--------- | -----------
firstName | The users first name
lastName | The users last name
phone | The users phone number
email | The users email
